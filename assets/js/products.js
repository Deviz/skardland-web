/**
 * Products class
 */
class Products {

    constructor() {
        //Init zoom plugin
        mediumZoom('[data-zoomable]');

        //Init Styles
        $('#loader').attr('style', 'display:none');
        $('#modal-default').attr('style', 'display:none');

        $( ".more-info" ).each(function(index) {
            $(this).on("click", function(){
                $('#loader').attr('style', 'display:block');
                $('#modal-content').attr('style', 'display:none');

                let id = $(this).attr('id').replace( /^\D+/g, '');
                var d = new FormData;
                d.append('id', id);

                fetch(details_link, {
                    method: 'POST',
                    credentials: 'include',
                    body: d
                }).then(d => d.text()).catch(d => console.log('Error:' + d)).then(d => {
                    $('#modal-content').html(d).attr('style', 'display:block');
                    $('#loader').attr('style', 'display:none');
                })
            });
        });
    }
}

export default new Products();