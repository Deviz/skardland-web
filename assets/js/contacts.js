/**
 * MapBox map class
 */
class Contacts {

    constructor() {
        //Variables
        this.accessToken = 'insert token here';
        this.maxZoom = 18;

        //Init
        this.initializeMap([52, 25]);
        this.addTileLayer();

        //Markers
        this.addMarker([55, 30]).bindPopup("Vilniaus g. 456, Vilniaus raj.");
        this.addMarker([50, 20]).bindPopup("Kauno g. 786, Kauno raj.");
    }

    initializeMap(center,  map_id = 'map', zoom = 10) {
        this.map = L.map(map_id)
            .setView(center, zoom);

        this.map.scrollWheelZoom.disable();
        this.map.on('click', () => { this.map.scrollWheelZoom.enable();});
        this.map.on('mouseout', () => { this.map.scrollWheelZoom.disable();});
    };

    addTileLayer(id = 'mapbox.streets') {
        L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            maxZoom: this.maxZoom,
            id: id,
            accessToken: this.accessToken
        }).addTo(this.map).setZIndex();
    };

    addMarker(latLng) {
        return L.marker(latLng).addTo(this.map);
    }

}

export default new Contacts();