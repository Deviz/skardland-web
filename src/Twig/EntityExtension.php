<?php

namespace App\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class EntityExtension extends AbstractExtension
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('getEntities', [$this, 'getEntities']),
            new TwigFunction('getEntitiesBy', [$this, 'getEntitiesBy']),
        ];
    }

    public function getEntities($value)
    {
        return $this->em->getRepository('App:'.$value)->findAll();
    }

    public function getEntitiesBy($entity, $key, $value)
    {
        return $this->em->getRepository('App:'.$entity)
            ->findBy([$key => $value]);
    }
}
