<?php

namespace App\Twig;

use App\Utils\Slugify;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SlugifyExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('slugify', [$this, 'convert']),
        ];
    }

    public function convert($text)
    {
        return Slugify::convert($text);
    }
}
