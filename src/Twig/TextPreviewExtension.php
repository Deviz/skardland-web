<?php

namespace App\Twig;

use App\Utils\TextPreview;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class TextPreviewExtension extends AbstractExtension
{

    public function getFunctions(): array
    {
        return [
            new TwigFunction('tp', [$this, 'tp']),
        ];
    }

    public function tp($text)
    {
        $tp = new TextPreview(60,3);

        return $tp->makePreview($text);
    }
}
