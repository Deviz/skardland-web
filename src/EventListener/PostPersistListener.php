<?php
namespace App\EventListener;

use App\Entity\Category;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\Product;

class PostPersistListener
{
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Category)
            $this->setAndSaveData($args, 'Category');

        elseif ($entity instanceof Product)
            $this->setAndSaveData($args, 'Product');

    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function setAndSaveData($args, $string)
    {
        $entity = $args->getObject();
        $em = $args->getObjectManager();

        /**
         * @var Category|Product $previous
         */
        $previous = $em->getRepository('App:'.$string)
            ->findBy([], ['id' => 'DESC'])[0];

        if ($previous === null)
            $entity->setPosition(
                1
            );
        else
            $entity->setPosition(
                $previous->getPosition() + 1
            );

        $em->persist($entity);
        $em->flush();
    }
}