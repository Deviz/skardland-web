<?php

namespace App\Service;

use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvService
{
    public function getResponseFromQueryBuilder(QueryBuilder $queryBuilder, $columns, $filename)
    {
        $entities = new ArrayCollection($queryBuilder->getQuery()->getResult());
        $response = new StreamedResponse();

        if (is_string($columns)) {
            $columns = $this->getColumnsForEntity($columns);
        }

        $response->setCallback(function () use ($entities, $columns) {
            $handle = fopen('php://output', 'w+');
            // Add header
            fputcsv($handle, array_keys($columns));
            while ($entity = $entities->current()) {
                $values = [];
                foreach ($columns as $column => $callback) {
                    $value = $callback;
                    if (is_callable($callback)) {
                        $value = $callback($entity);
                    }
                    $values[] = $value;
                }
                fputcsv($handle, $values);
                $entities->next();
            }
            fclose($handle);
        });

        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        return $response;
    }
    private function getColumnsForEntity($class)
    {
        $columns[Product::class] = [
            'ID' => function (Product $product) {
                return $product->getId();
            },
            'Position' => function (Product $product) {
                return $product->getPosition();
            },
            'Title' => function (Product $product) {
                return $product->getTitle();
            },
            'Description' => function (Product $product) {
                return $product->getDescription();
            },
            'Price' => function (Product $product) {
                return $product->getPrice();
            },
            'Category' => function (Product $product) {
                return $product->getCategory()->getId();
            },
            'Image' => function (Product $product) {
                return $product->getImage();
            },
            // ...
        ];
        if (array_key_exists($class, $columns)) {
            return $columns[$class];
        }
        throw new \InvalidArgumentException(sprintf(
            'No columns set for "%s" entity',
            $class
        ));
    }
}