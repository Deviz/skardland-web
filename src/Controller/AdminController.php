<?php

namespace App\Controller;

use App\Service\CsvService;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController as BaseAdminController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class AdminController extends BaseAdminController
{
    private $csvService;

    public function __construct(CsvService $csvService)
    {
        $this->csvService = $csvService;
    }

    public function exportAction()
    {
        $sortDirection = $this->request->query->get('sortDirection');

        if (empty($sortDirection) || !in_array(strtoupper($sortDirection), ['ASC', 'DESC'])) {
            $sortDirection = 'DESC';
        }

        $queryBuilder = $this->createListQueryBuilder(
            $this->entity['class'],
            $sortDirection,
            $this->request->query->get('sortField'),
            $this->entity['list']['dql_filter']
        );

        return $this->csvService->getResponseFromQueryBuilder(
            $queryBuilder,
            $this->getDoctrine()->getRepository($this->entity['class'])->getClassName(),
            $this->entity['name'].'.csv'
        );
    }

    public function importAction()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_FILES['files'])) {
                $errors = [];
                $path = 'uploads/';
                $extensions = ['jpg', 'jpeg', 'png', 'gif'];

                $all_files = count($_FILES['files']['tmp_name']);

                for ($i = 0; $i < $all_files; $i++) {
                    $file_name = $_FILES['files']['name'][$i];
                    $file_tmp = $_FILES['files']['tmp_name'][$i];
                    $file_type = $_FILES['files']['type'][$i];
                    $file_size = $_FILES['files']['size'][$i];
                    $file_ext = strtolower(end(explode('.', $_FILES['files']['name'][$i])));

                    $file = $path . $file_name;

                    if (!in_array($file_ext, $extensions)) {
                        $errors[] = 'Extension not allowed: ' . $file_name . ' ' . $file_type;
                    }

                    if ($file_size > 2097152) {
                        $errors[] = 'File size exceeds limit: ' . $file_name . ' ' . $file_type;
                    }

                    if (empty($errors)) {
                        return new JsonResponse('ok');
                    }
                }
                return new JsonResponse('not ok');

                //if ($errors) print_r($errors);
            }
        }
    }
}