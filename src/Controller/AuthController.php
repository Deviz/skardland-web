<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class AuthController extends AbstractController
{
    /**
     * @Route("/auth", name="auth")
     *
     * @param AuthenticationUtils $authUtils
     * @return Response
     */
    public function login(
        AuthenticationUtils $authUtils
    ): Response
    {
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        return $this->render('auth/login.html.twig', array(
            'error' => $error
        ));
    }

}
