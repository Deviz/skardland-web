<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $categories = $this->getDoctrine()->getManager()
            ->getRepository('App:Category')->findBy(
                [], ['position' => 'DESC']
            );

        return $this->render('main/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/kontaktai/", name="contacts")
     */
    public function contacts(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('name', TextType::class,
                [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'form.name'
                    ]
                ]
            )
            ->add('email', EmailType::class,
                [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'form.email'
                    ]
                ]
            )
            ->add('message', TextareaType::class,
                [
                    'label' => false,
                    'attr' => [
                        'class' => 'form-control',
                        'placeholder' => 'form.message',
                        'style' => 'height:150px'
                    ]
                ]
            )
            ->add('submit', SubmitType::class,
                [
                    'label' => 'form.send',
                    'attr' => [
                        'class' => 'btn btn-primary'
                    ]
                ]
            )
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            $transport = (new \Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'))
                ->setUsername(getenv('MAILER_USERNAME'))
                ->setPassword(getenv('MAILER_PASSWORD'));

            $mailer = new \Swift_Mailer($transport);
            $message = (new \Swift_Message('Žinutė nuo - '.$data['name']))
                ->setFrom([$data['email']])
                ->setTo('info@skardland.lt')
                ->setBody($data['message']);

            if (!$mailer->send($message))
                $this->addFlash('danger', 'Įvyko klaida siunčiat laišką!');

            $this->addFlash('success', 'Jūsų laiškas buvo sėkmingai išsiųstas!');
        }

        return $this->render(
            "main/contacts.html.twig",
            [
                'form' => $form->createView()
            ]
        );
    }

    /**
     * @Route("/apie/", name="about")
     */
    public function about()
    {
        return $this->render('main/about.html.twig');
    }

    /**
     * @Route("/slapuku-politika/", name="privacy")
     */
    public function privacy()
    {
        return $this->render('main/privacy.html.twig');
    }
}
