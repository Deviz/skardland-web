<?php

namespace App\Controller;

use App\Entity\Category;
use Pagerfanta\Adapter\ArrayAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractController
{
    /**
     * @Route("/produktai/{category}-{slug}/", name="products")
     * @Route("/produktai/{category}-{slug}/{page}/", requirements={"page": "[1-9]\d*"}, name="products-paginated")
     */
    public function products(Category $category, int $page = 1)
    {
        $products = $this->getDoctrine()->getRepository('App:Product')->findBy(
            ['category' => $category->getId()], ['position' => 'DESC']
        );

        return $this->render('products/index.html.twig', [
            'category' => $category,
            'products' => $this->createPaginator($products, $page)
        ]);
    }

    /**
     * @Route("/details/", name="product-details")
     */
    public function details(Request $request)
    {
        $product = $this->getDoctrine()->getRepository('App:Product')->find(
            (int) $request->get('id')
        );

        return $this->render('products/details.html.twig', [
            'product' => $product
        ]);
    }

    private function createPaginator($arr, int $page, int $max = 9): Pagerfanta
    {
        $paginator = new Pagerfanta(
            new ArrayAdapter($arr)
        );

        $paginator
            ->setMaxPerPage($max)
            ->setCurrentPage($page);

        return $paginator;
    }
}
