<?php
/**
 * Created by PhpStorm.
 * User: Deividas
 * Date: 2018-02-13
 * Time: 17:40
 */

namespace App\Utils;

class TextPreview
{
    protected $maxCharacters;
    protected $maxLines;
    protected $encoding;
    protected $lineBreaks;

    public function __construct($maxCharacters = 350, $maxLines = 5, $encoding = 'UTF-8', array $lineBreaks = ["\r\n", "\r", "\n"])
    {
        $this->maxCharacters = $maxCharacters;
        $this->maxLines = $maxLines;
        $this->encoding = $encoding;
        $this->lineBreaks = $lineBreaks;
    }

    public function makePreview($text)
    {
        $text = $this->normalizeLinebreaks($text);

        // this prevents the breaking of the &quote; etc
        $text = html_entity_decode($text, ENT_QUOTES, $this->encoding);

        $text = $this->limitLines($text);

        if (mb_strlen($text, $this->encoding) > $this->maxCharacters) {
            $text = $this->limitCharacters($text);
        }

        return html_entity_decode($text, ENT_QUOTES, $this->encoding);
    }

    protected function normalizeLinebreaks($text)
    {
        return str_replace($this->lineBreaks, "\n", $text);
    }

    protected function limitLines($text)
    {
        $lines = explode("\n", $text);
        $limitedLines = array_slice($lines, 0, $this->maxLines);

        return implode("\n", $limitedLines);
    }

    protected function limitCharacters($text)
    {
        return substr($text, 0, $this->maxCharacters);
    }
}