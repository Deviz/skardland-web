<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Setting;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadSettings($manager);
        $this->loadCategories($manager);
    }

    private function loadUsers(ObjectManager $manager)
    {
        foreach ($this->getUserData() as [$username, $password, $email, $roles]) {
            $user = new User();
            $user
                ->setUsername($username)
                ->setEmail($email)
                ->setRoles($roles)
                ->setPassword($this->passwordEncoder->encodePassword(
                    $user,
                    $password
                ));

            $manager->persist($user);
            $this->addReference($username, $user);
        }
        $manager->flush();
    }

    private function loadSettings(ObjectManager $manager)
    {
        foreach ($this->getSettingData() as [$name, $value]) {
            $setting = new Setting($name, $value);

            $manager->persist($setting);
        }
        $manager->flush();
    }

    private function loadCategories(ObjectManager $manager)
    {
        foreach ($this->getCategoriesData() as [$title, $content, $icon]) {
            $category = new Category();

            $category
                ->setTitle($title)
                ->setContent($content)
                ->setIcon($icon);

            $manager->persist($category);
        }
        $manager->flush();
    }

    private function getUserData(): array
    {
        return [
            ['admin', '123456', 'admin@gmail.com', ['ROLE_ADMIN']]
        ];
    }

    private function getSettingData(): array
    {
        return [
            ['title', 'Skardland.lt - profesionalias skardos lankstinių gamybos ir montavimo paslaugas už priimtiną kainą.'],
            ['meta-description', 'Mes Jums siūlome profesionalias skardos lankstinių gamybos ir montavimo paslaugas už priimtiną kainą.'],
            ['meta-keywords', 'skardland.lt,skardland,skardos lankstiniai,skardines tvoros'],
            ['fb', 'https://www.facebook.com/'],
            ['phone-number', '+370 888 999 55'],
            ['address-1', 'Vilniaus g. 456, Vilniaus raj.'],
            ['address-2', 'Kauno g. 786, Kauno raj.'],
            ['email', 'info@skardland.lt'],
        ];
    }

    private function getCategoriesData(): array
    {
        return [
            ['Skardos lankstiniai stogui',
                'Standartiniai ir nestandartiniai skardos lankstiniai stogui.
                 Mes siūlome Jums įvairių matmenų skardos lankstinius. 
                 Gaminame indvidualiai kiekvienam stogui.', 'fas fa-home'],

            ['Skardinės tvoros', 'Mūsų skardinės tvoros skydai užtikrina išskirtinį patvarumą. 
            Paprastas montavimas, platus spalvų pasirinkimas...', 'fas fa-align-justify'],

            ['Kiti lankstiniai', 'Čia rasite daugybę įvarių lankstinių.', 'fas fa-archway']
        ];
    }

}
