### Installing application
Node.js, Composer and Yarn required.

Installing assets:
```sh
$ composer install
$ yarn install
```

Compiling assets:
```sh
# compile assets once
$ yarn encore dev

# or, recompile assets automatically when files change
$ yarn encore dev --watch

# on deploy, create a production build
$ yarn encore production

# Install EasyAdminBundle and Pagerfanta styles
$ php bin/console assets:install
```

Setting up database:
```sh
$ php bin/console doctrine:fixtures:load
# $ php bin/console d:m:d
# $ php bin/console d:m:m
```

Default login:
```sh
/_administravimas

# admin@gmail.com
# 123456
```
